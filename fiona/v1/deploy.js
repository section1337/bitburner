/** @param {NS} ns **/

// Used for importing targets from other scripts
import { FileHandler } from "/data/file-handler.js";

export async function main(ns) {
    //Logs are nice to know whats going on
    ns.disableLog('sleep');
    ns.disableLog('getServerMaxRam');
    ns.disableLog('getServerUsedRam');
    ns.disableLog('getServerSecurityLevel');
    ns.disableLog('getServerMinSecurityLevel');
    ns.disableLog('getServerMaxMoney');
    ns.disableLog('getServerMoneyAvailable');
    ns.disableLog('getHackingLevel');
    // If you do not want an open tail window with information what this script is doing on every start, you can remove/comment the tail line below.
    //ns.tail();

    // Used for importing targets from other scripts
    const FILEHANDLER_SERVERDATABASE = new FileHandler(ns, "/database/serverdatabase.txt");
    const FILEHANDLER_TARGET = new FileHandler(ns, "/database/target.txt");

    const RAM_PER_THREAD = ns.getScriptRam("/shared/weaken.js");

    // So many weaken threads are needed to counter one hack/grow thread.
    let WEAKEN_PER_THREAD = ns.weakenAnalyze(1);
    let HW_THREADS = ns.hackAnalyzeSecurity(1) / WEAKEN_PER_THREAD;
    let GW_THREADS = ns.growthAnalyzeSecurity(1) / WEAKEN_PER_THREAD;

    const RESERVED_HOME_RAM = 50; //GB - change if you want to reserve more of you home ram
    const SLEEP_ITERATION = 10; // ms - reduce if you need to go even faster, or raise if you get lag spikes
    const PAUSE_ITERATION = 1; // seconds - Pause after the script did touch every server and every target - how often should it run again

    // This will be used to make sure that every batch goes through and is not blocked my "script already running with same arguments"
    let randomArgument = 1;


    while (true) {
        const MIN_PERCENT_MONEY = 10 / 100;
        const MIN_MONEY = Math.max(1, ns.getServerMaxMoney("home") * MIN_PERCENT_MONEY);
        const SERVERS = await FILEHANDLER_SERVERDATABASE.read();
        const PRIME_TARGET = await FILEHANDLER_TARGET.read();
        
        // If we are so early in the game that we can't even effectively farm ALL servers...
        const GOOD_HACK_LEVEL = 1500;
        let earlyGameCheck = false;
        if (ns.getHackingLevel() < GOOD_HACK_LEVEL && ns.serverExists(PRIME_TARGET.name) && PRIME_TARGET.root && PRIME_TARGET.maxMoney > 1)
            earlyGameCheck = true;

        for (let target of SERVERS) {
            await ns.sleep(SLEEP_ITERATION);
            // ...we stick with THE BEST one. Adjust the criteria to your liking.
            if (earlyGameCheck)
                target = PRIME_TARGET;
            else if(target.type != "target" || !ns.serverExists(target.name) || !target.root || target.maxMoney < MIN_MONEY)
                continue;

            const HACK_MONEY_PERCENTAGE = ns.hackAnalyze(target.name);
            let currentSecLevel = ns.getServerSecurityLevel(target.name);
            let availableMoney = Math.max(1, ns.getServerMoneyAvailable(target.name));
            let growthCalculated = false;
            let requiredGrowThreads = 0;
            let batchDelay = 0;

            for (const server of SERVERS) {
                await ns.sleep(SLEEP_ITERATION);
                if ( server.type == "target" || !ns.serverExists(server.name) || !server.root)
                    continue;
                //Calculate possible threads - If "home", we want some spare RAM
                let threadsToUse = Math.floor((server.maxRam - ns.getServerUsedRam(server.name) - (server.type == "home" ? RESERVED_HOME_RAM : 0)) / RAM_PER_THREAD);

                if (threadsToUse < 1) continue;

                // Home multi-core support
                let S_CORES = 1;
                if (server.type == "home") {
                    S_CORES = ns.getServer("home").cpuCores;
                    WEAKEN_PER_THREAD = ns.weakenAnalyze(1, S_CORES);
                    HW_THREADS = ns.hackAnalyzeSecurity(1) / WEAKEN_PER_THREAD;
                    GW_THREADS = ns.growthAnalyzeSecurity(1, target.name, S_CORES) / WEAKEN_PER_THREAD;
                }

                // Weaken the server down to minimum sec level
                if (currentSecLevel > target.minSec) {
                    const REDUCED_SEC_LEVEL = WEAKEN_PER_THREAD * threadsToUse;
                    // Only use needed threads
                    if (currentSecLevel - REDUCED_SEC_LEVEL < target.minSec) {
                        threadsToUse = Math.ceil((currentSecLevel - target.minSec) / WEAKEN_PER_THREAD);
                        currentSecLevel = target.minSec;
                    }
                    else
                        currentSecLevel -= REDUCED_SEC_LEVEL;
                    ns.exec("/shared/weaken.js", server.name, threadsToUse, target.name, 0, randomArgument++);
                }
                // Grow the server to the maximum money
                else if (availableMoney < target.maxMoney && (requiredGrowThreads != 0 || !growthCalculated)) {
                    if (!growthCalculated) {
                        requiredGrowThreads = Math.ceil(ns.growthAnalyze(target.name, target.maxMoney / availableMoney, S_CORES));
                        growthCalculated = true;
                    }
                    // Do not use more than needed
                    threadsToUse = Math.min(requiredGrowThreads, threadsToUse)
                    // Save still needed threads, if any, for this iteration    
                    requiredGrowThreads -= threadsToUse;

                    // Factor in the raise in security level.
                    currentSecLevel += ns.growthAnalyzeSecurity(threadsToUse, target.name, S_CORES);
                    ns.exec("/shared/grow.js", server.name, threadsToUse, target.name, 0, randomArgument++);
                }
                // Fully prepped - Let's do batching
                else {
                    if (HACK_MONEY_PERCENTAGE == 0) continue;

                    // Thread calculation
                    let hackThreads = Math.ceil(threadsToUse / 8);
                    let growThreads, weakenThreads, weakenThreads2, goLower = false, threadUsage;
                    while (true) {
                        if (HACK_MONEY_PERCENTAGE * hackThreads > 1)
                            hackThreads = Math.ceil(1 / HACK_MONEY_PERCENTAGE);

                        // Calculate grow threads and weaken threads for the hackThreads amount
                        growThreads = Math.ceil(ns.growthAnalyze(target.name, 1 / (1 - Math.min(0.99, HACK_MONEY_PERCENTAGE * hackThreads)), S_CORES));
                        weakenThreads = Math.ceil(HW_THREADS * hackThreads);
                        weakenThreads2 = Math.max(1, Math.ceil(GW_THREADS * growThreads)); // GW_THREADS could be 0

                        // How much percent of threads would this take?
                        threadUsage = (hackThreads + growThreads + weakenThreads + weakenThreads2) / threadsToUse;

                        if (HACK_MONEY_PERCENTAGE * hackThreads >= 1)
                            break;

                        // If too much, reduce and calculate again, or stop if we cant go lower.
                        if (threadUsage > 1) {
                            if (hackThreads > 1) {
                                //hackThreads -= Math.max(1, hackThreads * (threadUsage - 1));
                                hackThreads--;
                                goLower = true;
                            }
                            else break;
                        }
                        // If we have enough free processes, lets raise hackThreads and calculate again
                        else if (Math.floor((1 - threadUsage) * hackThreads) > 1) {
                            hackThreads += Math.floor((1 - threadUsage) * hackThreads / 2);
                            if (goLower) break; //This is to prevent a softlock going up and down 1 thread.                                
                        }
                        else
                            // This is perfect. Get out with perfect thread amounts.
                            break;
                        await ns.sleep(2);
                    }

                    let THREAD_DELAY = 100; //ms
                    ns.exec("/shared/weaken.js", server.name, weakenThreads, target.name, batchDelay, randomArgument);
                    ns.exec("/shared/weaken.js", server.name, weakenThreads2, target.name, batchDelay + THREAD_DELAY * 2, randomArgument);
                    ns.exec("/shared/grow.js", server.name, growThreads, target.name, batchDelay + THREAD_DELAY + ns.getWeakenTime(target.name) - ns.getGrowTime(target.name), randomArgument);
                    ns.exec("/shared/hack.js", server.name, hackThreads, target.name, batchDelay - THREAD_DELAY + ns.getWeakenTime(target.name) - ns.getHackTime(target.name), randomArgument++);
                    // If we would fire the next HWGW without this batchDelay, they might intersect
                    batchDelay += 4 * THREAD_DELAY;
                }
            }
        }
        await ns.sleep(PAUSE_ITERATION * 1000);
    }
}