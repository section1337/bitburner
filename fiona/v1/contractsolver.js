/** @param {NS} ns */
export async function main(ns) {
	const contract = ns.args[0];
	const server = ns.args[1];
	const type = ns.codingcontract.getContractType(contract, server);
	const data = ns.codingcontract.getData(contract, server);
	//ns.tprint(server + " - Type:" + type + " - " + data);
	const answer = await contractSolver(ns, type, data);
	if (true && answer != null) {
		ns.tprint("INFO: Answer: " + answer);
		ns.tprint("INFO: Attempts: " + ns.codingcontract.getNumTriesRemaining(contract, server));
		const solvingResult = ns.codingcontract.attempt(answer, contract, server, { returnReward: true })
		ns.tprint("INFO: " + solvingResult);
	}
	else {
		//ns.tprint(server + " - Type:" + type + " - " + data);
	}
	// ns.tprint("TEST");
	// const temp = await lzdecomp("5aaabb450723abb", ns);
	// ns.tprint("ERROR - lzd: " + temp);
}

async function contractSolver(ns, ftype, fdata) {
	await ns.sleep(1);
	let array;
	switch (ftype) {
		case "Array Jumping Game":
			return arrayjumps(fdata, 0).toString();
		case "Array Jumping Game II":
			let result = arrayjumpsII(fdata, 0);
			//More jumps = did not reach = 0
			if (result > fdata.length)
				result = 0;
			return result.toString();
		case "Generate IP Addresses":
			return generateIps(ns, fdata);
		case "Unique Paths in a Grid I":
			return uniquePathsI(fdata);
		case "Total Ways to Sum":
			return null;
		case "Total Ways to Sum II":
			return null;
		case "Compression I: RLE Compression":
			let result1 = '';
			if (fdata.length > 0) {
				let count = 1;
				let value = fdata[0];
				let entry;
				for (let i = 1; i < fdata.length; ++i) {
					entry = fdata[i];
					if (entry == value && count < 9) {
						count += 1;
					} else {
						result1 += count + '' + value;
						count = 1;
						value = entry;
					}
					await ns.sleep(1);
				}
				result1 += count + '' + entry;
			}
			return result1;
		// joesguns - Data:2,1,4,4,4,2,1,5,2,0,1
		case "Subarray with Maximum Sum":
			let maxsum = 0;
			for (let i = 0; i < fdata.length; ++i) {
				let currsum = 0;
				for (let j = i; j < fdata.length; ++j) {
					currsum += fdata[j];
					if (currsum > maxsum)
						maxsum = currsum;
					await ns.sleep(1);
				}
				await ns.sleep(1);
			}
			return maxsum;
		case "Merge Overlapping Intervals":
			let intervals = fdata.slice();
			intervals.sort(function (a, b) {
				return a[0] - b[0];
			})
			let result2 = [];
			let start = intervals[0][0];
			let end = intervals[0][1];
			for (const interval of intervals) {
				if (interval[0] <= end) {
					end = Math.max(end, interval[1])
				} else {
					result2.push([start, end])
					start = interval[0]
					end = interval[1]
				}
				await ns.sleep(1);
			}
			result2.push([start, end])
			const sanitizedResult = convert2DArrayToString(result2)
			return sanitizedResult;
		case "Sanitize Parentheses in Expression":
			let open = 0;
			array = fdata.split('');
			for (let i = 0; i < array.length; ++i) {
				let remove = true;
				if (array[i] == "(") {
					for (let j = i; j < array.length; ++j) {
						if (array[j] == ")")
							remove = false;
					}
					if (remove) {
						array.splice(i, 1);
						i--;
					}
					else
						open++;
				}
				else if (array[i] == ")") {
					for (let j = 0; j < i; ++j) {
						if (array[j] == "(")
							remove = false;
					}
					if (remove) {
						array.splice(i, 1);
						i--;
					}
					else if (open > 0)
						open--;
					else
						return null;
				}
				await ns.sleep(1);
			}
			if (open > 0)
				return null;
			return array.join('');
		case "Algorithmic Stock Trader I":
			let min = 99999;
			let bestprofit = 0;
			for (let i = 0; i < fdata.length; ++i) {
				if (fdata[i] < min)
					min = fdata[i];
				if (fdata[i] - min > bestprofit)
					bestprofit = fdata[i] - min;
				await ns.sleep(1);
			}
			return bestprofit;

		case "Algorithmic Stock Trader IV":
			return null;
			let sales = Math.ceil(fdata[0] / 2);
			let trades;
			for (let i = 0; i < fdata[1].length; ++i) {
				await ns.sleep(1);

			}
		//[4, [191,143,112,164,92,46,134,46,107,47,83,43,188,75,159,72,151,114,129,169,105,21,19,129,28,176,12,71,130,93,23,155,153,64,38,24,86,77,181,67,112,24,121,33,151,88,186,27]]
		case "Unique Paths in a Grid II":
			return uniquePathsII(fdata, 0, 0).toString();
		case "Spiralize Matrix":
			return await spiral(ns, fdata);
		case "Minimum Path Sum in a Triangle":
			return trianglesum(fdata, 0, 0).toString();
		case "Find Largest Prime Factor":
			return await primedivision(ns, fdata);

	}
	return null;
}

/*
Total Ways to Sum II
You are attempting to solve a Coding Contract. You have 10 tries remaining, after which the contract will self-destruct.


How many different distinct ways can the number 46 be written as a sum of integers contained in the set:

[2,3,4,5,7,9,10,11,13]?

You may use each integer in the set zero or more times.

{
	name: 'Total Ways to Sum',
	solver: function (data) {
		const ways = [1]
		ways.length = data + 1
		ways.fill(0, 1)
		for (let i = 1; i < data; ++i) {
			for (let j = i; j <= data; ++j) {
				ways[j] += ways[j - i]
			}
		}
		return ways[data]
	},
},
{
	name: 'Total Ways to Sum II',
	solver: function (data) {
		const n = data[0];
		const s = data[1];
		const ways = [1];
		ways.length = n + 1;
		ways.fill(0, 1);
		for (let i = 0; i < s.length; i++) {
			for (let j = s[i]; j <= n; j++) {
				ways[j] += ways[j - s[i]];
			}
		}
		return ways[n];
	},
},

*/
async function sum(sumpart, ns) {
	await ns.sleep(1);
	let sumcount = 0;
	return sumcount;
}

function convert2DArrayToString(arr) {
	const components = []
	arr.forEach(function (e) {
		let s = e.toString()
		s = ['[', s, ']'].join('')
		components.push(s)
	})
	return components.join(',').replace(/\s/g, '')
}

async function spiral(ns, field) {
	await ns.sleep(1);
	let result = [];
	let border = Math.ceil(Math.max(field[0].length, field.length) / 2);
	for (var j = 0; j < border && field[j]; j++) {
		let arrlength = field[j].length - 1;
		for (var i = 0; i < arrlength; i++) {
			result = result.concat(field[j].splice(0, 1));
		}
		arrlength = field.length - 1;
		for (var i = 0; i < arrlength; i++) {
			result = result.concat(field[i].splice(field[0].length - 1, 1));
		}
		arrlength = field[field.length - 1 - j].length - 1;
		for (var i = 0; i < arrlength; i++) {
			result = result.concat(field[field.length - 1 - j].splice(field[field.length - 1 - j].length - 1, 1));
		}
		for (var i = field.length - 1; i >= 0; i--) {
			result = result.concat(field[i].splice(0, 1));
		}
		await ns.sleep(1);
	}
	return result;
}

/*
Total Ways to Sum II
You are attempting to solve a Coding Contract. You have 10 tries remaining, after which the contract will self-destruct.
How many different distinct ways can the number 197 be written as a sum of integers contained in the set:
[4,5,8,12,15,16,17,19,20,21]?
You may use each integer in the set zero or more times.
*/

function trianglesum(field, row, count) {
	//Reached last line
	if (row == field.length - 1)
		return field[row][count];
	let minPath = 99999;
	let subPath = trianglesum(field, row + 1, count);
	minPath = subPath < minPath ? subPath : minPath;
	subPath = trianglesum(field, row + 1, count + 1);
	minPath = subPath < minPath ? subPath : minPath;
	return minPath + field[row][count];
}

/*
Compression II: LZ Decompression
You are attempting to solve a Coding Contract. You have 10 tries remaining, after which the contract will self-destruct.
Lempel-Ziv (LZ) compression is a data compression technique which encodes data using references to earlier parts of the data. In this variant of LZ, data is encoded in two types of chunk. Each chunk begins with a length L, encoded as a single ASCII digit from 1 - 9, followed by the chunk data, which is either:
1. Exactly L characters, which are to be copied directly into the uncompressed data.
2. A reference to an earlier part of the uncompressed data. To do this, the length is followed by a second ASCII digit X: each of the L output characters is a copy of the character X places before it in the uncompressed data.
For both chunk types, a length of 0 instead means the chunk ends immediately, and the next character is the start of a new chunk. The two chunk types alternate, starting with type 1, and the final chunk may be of either type.
You are given the following LZ-encoded string:
	26V7156DVV3665z1z1z9666FKF7X454U6W69605896FxRQU0Pv13
Decode it and output the original string.
Example: decoding '5aaabb450723abb' chunk-by-chunk
	5aaabb           ->  aaabb
	5aaabb45         ->  aaabbaaab
	5aaabb450        ->  aaabbaaab
	5aaabb45072      ->  aaabbaaababababa
	5aaabb450723abb  ->  aaabbaaababababaabb
*/

async function lzdecomp(string, ns) {
	await ns.sleep(1);
	ns.tprint("DEBUG");
	let resultstring = "";
	while (string.length > 1) {
		let length = +string.slice(0, 1);
		resultstring += string.slice(0, length);
		length = +string.slice(0, 1);
		if (length != 0) {
			let x = string.slice(0, 1);
			for (let i = 0; i < length; i++) {
				resultstring += resultstring[resultstring.length - 1 - x];
			}
		}
		await ns.sleep(1);
	}
	return resultstring;
}

async function primedivision(ns, number) {
	await ns.sleep(1);
	let highestfactor = 1;
	for (let i = 2; i <= number;) {
		if (number / i % 1 == 0) {
			number /= i;
			highestfactor = i;
		}
		else
			i++;
		await ns.sleep(1);
	}
	return highestfactor;
}
/*
Find Largest Prime Factor
You are attempting to solve a Coding Contract. You have 10 tries remaining, after which the contract will self-destruct.
A prime factor is a factor that is a prime number. What is the largest prime factor of 792882224?
*/

function arrayjumps(field, count) {
	if (count >= field.length - 1)
		return 1;
	for (let i = 1; i <= field[count]; i++) {
		if (arrayjumps(field, count + i) == 1)
			return 1;
	}
	return 0;
}

function arrayjumpsII(field, count) {
	if (count >= field.length - 1)
		return 0;
	let minJumps = 999;
	for (let i = 1; i <= field[count]; i++) {
		const jumps = arrayjumpsII(field, count + i);
		if (minJumps > 1 + jumps)
			minJumps = 1 + jumps;
	}
	return minJumps;
}

/*
Shortest Path in a Grid
You are attempting to solve a Coding Contract. You have 10 tries remaining, after which the contract will self-destruct.
You are located in the top-left corner of the following grid:
  [[0,0,0,0,0,0,0,0,1,0,0,1],
   [0,0,0,0,0,1,0,0,1,0,0,1],
   [0,0,1,0,0,1,1,1,1,1,1,0],
   [0,1,0,0,0,1,1,0,0,1,0,1],
   [0,0,0,0,0,0,0,0,0,0,0,0],
   [0,1,0,0,0,1,0,1,1,0,1,0],
   [1,0,0,1,0,0,1,0,0,0,0,0],
   [0,0,0,0,0,0,0,0,0,0,0,1],
   [1,0,0,0,0,1,0,1,1,0,0,0]]
You are trying to find the shortest path to the bottom-right corner of the grid, but there are obstacles on the grid that you cannot move onto. These obstacles are denoted by '1', while empty spaces are denoted by 0.
Determine the shortest path from start to finish, if one exists. The answer should be given as a string of UDLR characters, indicating the moves along the path
NOTE: If there are multiple equally short paths, any of them is accepted as answer. If there is no path, the answer should be an empty string.
NOTE: The data returned for this contract is an 2D array of numbers representing the grid.
Examples:
	[[0,1,0,0,0],
	 [0,0,0,1,0]]
Answer: 'DRRURRD'
	[[0,1],
	 [1,0]]
Answer: ''
*/

function shortestPath(field, row, column) {

}

function uniquePathsII(field, row, column) {
	//If out of boundaries or on a 1, this path did not work
	if (row == field.length || column == field[0].length || field[row][column] == 1)
		return 0;
	//If landed on goal field, return 1, path worked
	if (row == field.length - 1 && column == field[0].length - 1)
		return 1;
	//In all other cases return the paths of right and down paths
	return uniquePathsII(field, row + 1, column) + uniquePathsII(field, row, column + 1);
}

//C
async function generateIps(ns, num) {
	await ns.sleep(1);
	num = num.toString();

	const length = num.length;
	const ips = [];

	for (let i = 1; i <= 3 && i < length - 2; i++) {
		for (let j = i + 1; j <= i + 3 && j < length - 1; j++) {
			for (let k = j + 1; k <= j + 3 && k < length; k++) {
				const tempip = [
					num.slice(0, i),
					num.slice(i, j),
					num.slice(j, k),
					num.slice(k, length)
				];
				let isValid = true;
				tempip.forEach(seg => {
					isValid = isValid && isValidIpSegment(seg);
				});
				if (isValid) ips.push(tempip.join("."));
				await ns.sleep(1);
			}
			await ns.sleep(1);
		}
		await ns.sleep(1);
	}
	return ips;

}
//C
function isValidIpSegment(segment) {
	if (segment[0] == "0" && segment != "0") return false;
	segment = Number(segment);
	if (segment < 0 || segment > 255) return false;
	return true;
}
//C
function uniquePathsI(grid) {
	const rightMoves = grid[0] - 1;
	const downMoves = grid[1] - 1;
	return Math.round(factorialDivision(rightMoves + downMoves, rightMoves) / factorialDivision(downMoves, 1));
}
//C
function factorialDivision(n, d) {
	if (n == 0 || n == 1 || n == d)
		return 1;
	return factorialDivision(n - 1, d) * n;
}