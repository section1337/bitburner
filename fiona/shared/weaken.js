/** @param {NS} ns **/
export async function main(ns) {
	const SLEEPMS = ns.args[1] || 1;
	await ns.sleep(SLEEPMS);
	await ns.weaken(ns.args[0]);
}