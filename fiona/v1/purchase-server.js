/** @param {NS} ns */
export async function main(ns) {
	let currentram = 4;
	const MAX_RAM = ns.getPurchasedServerMaxRam();
	const RAM_8_COST = ns.getPurchasedServerCost(8);
	const MAX_SERVER = ns.getPurchasedServerLimit();
	let newram, currentprice, money, name;
	if (ns.serverExists("pserv-0")) {
		currentram = ns.getServerMaxRam("pserv-0");
		// This is just here if you manage to fuck up the script in the middle of buying. Will buy remaining servers, but it honestly sucks.
		for (let i = 1; i < MAX_SERVER; ++i) {
			name = "pserv-" + i;
			if (!ns.serverExists(name)) {
				newram = 8;
				currentprice = RAM_8_COST * (MAX_SERVER - i) * 2;
				while (currentprice * 2 < ns.getServerMoneyAvailable("home") && newram != MAX_RAM) {
					newram = newram * 2;
					currentprice = currentprice * 2;
				}
				while (ns.purchaseServer(name, newram) != name) {
					ns.tprint("INFO - tried to buy " + name + ", but only got " + Math.floor(100 * ns.getServerMoneyAvailable("home") / currentprice) + "% of the money. will try again");
					await ns.sleep(60000);
				}
			}
			await ns.sleep(10);
		}
	}
	if (currentram == MAX_RAM)
		return;
	newram = currentram;

	currentprice = RAM_8_COST * currentram / 8 * MAX_SERVER * 2;
	money = ns.getServerMoneyAvailable("home");
	while (currentprice * 2 < money && newram < 1000 || currentprice * 20 < money && newram != MAX_RAM) {
		newram = newram * 2;
		currentprice = currentprice * 2;
	}
	if (newram > currentram) {
		for (let i = 0; i < MAX_SERVER; ++i) {
			name = "pserv-" + i;
			if (ns.serverExists(name)) {
				ns.killall(name);
				ns.deleteServer(name);
			}
			while (ns.purchaseServer(name, newram) != name) {
				ns.tprint("WARN - tried to buy " + name + ", but no money... will try again");
				await ns.sleep(60000);
			}
			await ns.scp([
				"/shared/allround-hack.js",
				"/shared/weaken.js",
				"/shared/grow.js",
				"/shared/hack.js"
			], name);
			await ns.sleep(10);
		}
		ns.tprint("INFO: Server upgrade just happened to new: " + newram + "GB RAM.");
	}
}