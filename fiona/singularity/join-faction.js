/** @param {NS} ns */
export async function main(ns) {
	ns.singularity.joinFaction(ns.args[0]);
	await ns.sleep(1);
}