/** @param {NS} ns */
export async function main(ns) {

	for (let sleeveNumber = 0; sleeveNumber < ns.sleeve.getNumSleeves(); sleeveNumber++) {
		const SLEEVE_STATS = ns.sleeve.getSleeveStats(sleeveNumber);
		if (SLEEVE_STATS.sync < 100) {
			ns.sleeve.setToSynchronize(sleeveNumber);
			await ns.sleep(1000);
			const SYNCDIFF = ns.sleeve.getSleeveStats(sleeveNumber).sync - SLEEVE_STATS.sync;
			if(SYNCDIFF > 0.01)
				return;
		}
		if (SLEEVE_STATS.shock > 50)
			ns.sleeve.setToShockRecovery(sleeveNumber);
		else if (!ns.gang.inGang())
			ns.sleeve.setToCommitCrime(sleeveNumber, "Homicide");
		else if (SLEEVE_STATS.shock == 0) {
			const AVAIL_AUGS = ns.sleeve.getSleevePurchasableAugs(sleeveNumber);
			if (AVAIL_AUGS.length > 9) {
				let augsToShop = [];
				for (const AUG of AVAIL_AUGS) {
					if (AUG.cost * 100 < ns.getServerMoneyAvailable("home"))
						augsToShop = augsToShop.concat(AUG.name);
				}
				if (augsToShop.length > 9) {
					for (const AUG of augsToShop) {
						ns.sleeve.purchaseSleeveAug(sleeveNumber, AUG);
					}
				}
			}
		}
	}
	// 	getInformation(sleeveNumber) 	Get information about a sleeve.
	// getSleeveAugmentations(sleeveNumber) 	Get augmentations installed on a sleeve.
	// getSleevePurchasableAugs(sleeveNumber) 	List purchasable augs for a sleeve.
	// getSleeveStats(sleeveNumber) 	Get the stats of a sleeve.
	// getTask(sleeveNumber) 	Get task of a sleeve.
	// purchaseSleeveAug(sleeveNumber, augName) 	Purchase an aug for a sleeve.
	// setToBladeburnerAction(sleeveNumber, action, contract) 	Set a sleeve to perform bladeburner actions.
	// setToCompanyWork(sleeveNumber, companyName) 	Set a sleeve to work for a company.
	// setToFactionWork(sleeveNumber, factionName, factionWorkType) 	Set a sleeve to work for a faction.
	// setToGymWorkout(sleeveNumber, gymName, stat) 	Set a sleeve to workout at the gym.
	// setToUniversityCourse(sleeveNumber, university, className) 	Set a sleeve to take a class at a university.
	// travel(sleeveNumber, cityName) 	Make a sleeve travel to another city.
}