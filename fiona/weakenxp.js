/** @param {NS} ns **/

// Used for importing targets from other scripts
import { FileHandler } from "/data/file-handler.js";

export async function main(ns) {
	//Logs are nice to know whats going on
	ns.disableLog('sleep');
	ns.disableLog('getServerMaxRam');
	ns.disableLog('getServerUsedRam');
	ns.disableLog('getServerSecurityLevel');
	ns.disableLog('getServerMinSecurityLevel');
	ns.disableLog('getServerMaxMoney');
	ns.disableLog('getServerMoneyAvailable');
	ns.disableLog('getHackingLevel');
	// If you do not want an open tail window with information what this script is doing on every start, you can remove/comment the tail line below.
	ns.tail();

	const FILEHANDLER_TARGET = new FileHandler(ns, "/database/xptarget.txt");

	const RAM_PER_THREAD = ns.getScriptRam("/shared/weaken.js");

	const SLEEP_ITERATION = 1; // ms - reduce if you need to go even faster, or raise if you get lag spikes

	// This will be used to make sure that every batch goes through and is not blocked my "script already running with same arguments"
	let randomArgument = 1;

	let servers = ns.getPurchasedServers();
	servers = servers.concat("home");
	const target = await FILEHANDLER_TARGET.read();
	if (!ns.serverExists(target) || !ns.hasRootAccess(target))
		return;

	const HOME_RAM = ns.getServerMaxRam("home") - 50;
	const SERVER_RAM = ns.getServerMaxRam("pserv-0");

	while (true) {
		for (let server of servers) {
			if (!ns.serverExists(server))
				continue;

			let threadsToUse = Math.floor(((server == "home" ? HOME_RAM : SERVER_RAM) - ns.getServerUsedRam(server)) / RAM_PER_THREAD);
			if (threadsToUse < 1) continue;

			ns.exec("/shared/weaken.js", server, threadsToUse, target, 0, randomArgument++);
			await ns.sleep(SLEEP_ITERATION);
		}
		await ns.sleep(SLEEP_ITERATION);
	}
}