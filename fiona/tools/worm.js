/** @param {NS} ns **/
let pservs = [];
export async function main(ns) {
	await ns.sleep(1);
	for (var i = 0; i < 25; i++)
		pservs = pservs.concat("pserv-" + i);
	await wormFunc(ns, "home", "");
}

async function wormFunc(ns, serv, oldserv) {
	await ns.sleep(1);
	const NEIGHBORS = ns.scan(serv);
	ns.connect(serv);
	for (const NEIGHBOR of NEIGHBORS) {
		if (pservs.includes(NEIGHBOR) || NEIGHBOR == oldserv) continue;
		await wormFunc(ns, NEIGHBOR, serv);
		ns.connect(serv);
	}
	if (!ns.getServer(serv).backdoorInstalled &&
		(!ns.args[0] || serv == ns.args[0]) &&
		serv != "home" && ns.hasRootAccess(serv) &&
		ns.getServerRequiredHackingLevel(serv) <= ns.getHackingLevel())
		await ns.installBackdoor();
}