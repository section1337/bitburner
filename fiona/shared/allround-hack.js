/** @param {NS} ns */
import { FileHandler } from "/data/file-handler.js";

export async function main(ns) {
    const FILEHANDLER_TARGET = new FileHandler(ns, "/database/target.txt");
    while (true) {
        await ns.sleep(10);
        const GREEDY_MODE = ns.getServerMoneyAvailable("home") < 1000000000;
        const target = await FILEHANDLER_TARGET.read();
        if (!target.root)
            continue;

        const SEC_LEV = ns.getServerSecurityLevel(target.name);
        if (SEC_LEV > target.minSec + 1 || (GREEDY_MODE && SEC_LEV > target.minSec + 5)) {
            await ns.weaken(target.name);
        } else {
            const MON = ns.getServerMoneyAvailable(target.name);
            if (MON < target.maxMoney * 0.9 || (GREEDY_MODE && MON < target.maxMoney * 0.5)) {
                await ns.grow(target.name);
            } else {
                await ns.hack(target.name);
            }
        }
    }
}