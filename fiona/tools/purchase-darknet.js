/** @param {NS} ns */
export async function main(ns) {
	if (ns.getServerMoneyAvailable("home") < 200000)
		return;
	ns.purchaseTor();
	if (!ns.fileExists("BruteSSH.exe") && ns.getServerMoneyAvailable("home") >= ns.getDarkwebProgramCost("BruteSSH.exe")) {
		ns.purchaseProgram("BruteSSH.exe");
		ns.tprint("YAY! Just bought BruteSSH.exe!");
	}
	if (!ns.fileExists("FTPCrack.exe") && ns.getServerMoneyAvailable("home") >= ns.getDarkwebProgramCost("FTPCrack.exe")) {
		ns.purchaseProgram("FTPCrack.exe");
		ns.tprint("YAY! Just bought FTPCrack.exe!");
	}
	if (!ns.fileExists("RelaySMTP.exe") && ns.getServerMoneyAvailable("home") >= ns.getDarkwebProgramCost("RelaySMTP.exe")) {
		ns.purchaseProgram("RelaySMTP.exe");
		ns.tprint("YAY! Just bought RelaySMTP.exe!");
	}
	if (!ns.fileExists("HTTPWorm.exe") && ns.getServerMoneyAvailable("home") >= ns.getDarkwebProgramCost("HTTPWorm.exe")) {
		ns.purchaseProgram("HTTPWorm.exe");
		ns.tprint("YAY! Just bought HTTPWorm.exe!");
	}
	if (!ns.fileExists("SQLInject.exe") && ns.getServerMoneyAvailable("home") >= ns.getDarkwebProgramCost("SQLInject.exe")) {
		ns.purchaseProgram("SQLInject.exe");
		ns.tprint("YAY! Just bought SQLInject.exe!");
	}
	if (!ns.fileExists("DeepscanV1.exe") && ns.getServerMoneyAvailable("home") >= ns.getDarkwebProgramCost("DeepscanV1.exe") + 10000000) {
		ns.purchaseProgram("DeepscanV1.exe");
		ns.tprint("YAY! Just bought DeepscanV1.exe!");
	}
	if (!ns.fileExists("AutoLink.exe") && ns.getServerMoneyAvailable("home") >= ns.getDarkwebProgramCost("AutoLink.exe")) {
		ns.purchaseProgram("AutoLink.exe");
		ns.tprint("YAY! Just bought AutoLink.exe!");
	}
	if (!ns.fileExists("DeepscanV2.exe") && ns.getServerMoneyAvailable("home") >= ns.getDarkwebProgramCost("DeepscanV2.exe") + 100000000) {
		ns.purchaseProgram("DeepscanV2.exe");
		ns.tprint("YAY! Just bought DeepscanV2.exe!");
	}
}