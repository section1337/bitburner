/** @param {NS} ns **/
export async function main(ns) {
	const SLEEPMS = ns.args[1] || 1;
	await ns.sleep(SLEEPMS);
	await ns.grow(ns.args[0]);
}