/** @param {NS} ns **/
import { FileHandler } from "/data/file-handler.js";
export async function main(ns) {
	const HACK_LEV = ns.getHackingLevel();
	const FILEHANDLER_SERVERS = new FileHandler(ns, "/database/serverdatabase.txt");
	const FILEHANDLER_PSERVERS = new FileHandler(ns, "/database/pservers.txt");
	const servers = await FILEHANDLER_SERVERS.read();
	const P_SERVERS = await FILEHANDLER_PSERVERS.read();
	const SCRIPT_RAM = ns.getScriptRam("/shared/allround-hack.js");

	for (let server of servers) {
		await ns.sleep(10);
		if (!ns.serverExists(server.name))
			continue;

		await ns.scp([
			"/shared/allround-hack.js",
			"/shared/weaken.js",
			"/shared/grow.js",
			"/shared/hack.js",
			"/data/file-handler.js",
			"/database/target.txt"
		], server.name);

		if (server.hackLev > HACK_LEV)
			continue;

		if (!ns.hasRootAccess(server.name)) {
			let openPorts = 0;
			if (ns.fileExists("BruteSSH.exe")) {
				ns.brutessh(server.name);
				openPorts++;
			}
			if (ns.fileExists("FTPCrack.exe")) {
				ns.ftpcrack(server.name);
				openPorts++;
			}
			if (ns.fileExists("RelaySMTP.exe")) {
				ns.relaysmtp(server.name);
				openPorts++;
			}
			if (ns.fileExists("HTTPWorm.exe")) {
				ns.httpworm(server.name);
				openPorts++;
			}
			if (ns.fileExists("SQLInject.exe")) {
				ns.sqlinject(server.name);
				openPorts++;
			}
			if (ns.getServerNumPortsRequired(server.name) <= openPorts) {
				ns.nuke(server.name);
			}
		}
		const S_FREERAM = server.maxRam - ns.getServerUsedRam(server.name);
		if (server.type == "target" && S_FREERAM >= SCRIPT_RAM) {
			ns.exec("/shared/allround-hack.js", server.name, Math.floor(S_FREERAM / SCRIPT_RAM));
		}
	}
}