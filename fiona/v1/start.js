/** @param {NS} ns */
import { FileHandler } from "/data/file-handler.js";
export async function main(ns) {
	ns.disableLog('sleep');
	ns.tail();
	const FILEHANDLER_GLOBAL_VARS = new FileHandler(ns, "/database/globalvars.txt");
	while (true) {
		let pid;

		pid = ns.run("helper.js");
		while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }

		const GLOBAL_VARS = await FILEHANDLER_GLOBAL_VARS.read();
		const IDLE_FOCUS = GLOBAL_VARS.IDLE_FOCUS;
		const STORY_PROGRESS = GLOBAL_VARS.STORY_PROGRESS;

		const CURR_HACK_LEVEL = ns.getHackingLevel();

		// SING - DARKNET
		if (!ns.fileExists("SQLInject.exe")) {
			pid = ns.run("tools/purchase-darknet.js");
			while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
		}
		// SING - UPGRADE HOME
		pid = ns.run("/singularity/upgrade-homeram.js");
		while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
		pid = ns.run("/singularity/upgrade-homecores.js");
		while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
		// FIND PSERVERS
		pid = ns.run("tools/get-purchased-servers.js");
		while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
		// SCAN / CONTRACTS / EARLY HACK
		pid = ns.run("/v1/servercrawler.js");
		while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }

		// OPEN SERVERS
		pid = ns.run("/v1/opener.js");
		while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
		// SET TARGET
		pid = ns.run("/v1/targeter.js");
		while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
		// SLEEVE MANAGEMENT
		pid = ns.run("/v1/sleeves.js");
		while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }

		//		S T O R Y - P R O G R E S S I O N
		if (STORY_PROGRESS) {
			if (CURR_HACK_LEVEL > 2500) {
				pid = ns.run("singularity/join-faction.js", 1, "Daedalus");
				while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
				pid = ns.run("singularity/workForFaction.js", 1, "Daedalus", "Hacking Contracts", IDLE_FOCUS);
				while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
			}
			else if (CURR_HACK_LEVEL > ns.getServerRequiredHackingLevel("run4theh111z")) {
				pid = ns.run("/tools/worm.js", 1, "run4theh111z");
				while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
				pid = ns.run("singularity/join-faction.js", 1, "BitRunners");
				while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
				pid = ns.run("singularity/workForFaction.js", 1, "BitRunners", "Hacking Contracts", IDLE_FOCUS);
				while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
			}
			else if (CURR_HACK_LEVEL > ns.getServerRequiredHackingLevel("I.I.I.I")) {
				pid = ns.run("/tools/worm.js", 1, "I.I.I.I");
				while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
				pid = ns.run("singularity/join-faction.js", 1, "The Black Hand");
				while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
				pid = ns.run("singularity/workForFaction.js", 1, "The Black Hand", "Hacking Contracts", IDLE_FOCUS);
				while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
			}
			else if (CURR_HACK_LEVEL > ns.getServerRequiredHackingLevel("avmnite-02h")) {
				// SING - GET SECOND PORT OPENER IF NOT BOUGHT ALREADY
				if (!ns.fileExists("FTPCrack.exe")) {
					pid = ns.run("singularity/program.js", 1, "FTPCrack.exe", IDLE_FOCUS);
					while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
				}
				// SING - JOIN NITESEC - WORK - PURCHASE AUGMENTS
				else {
					pid = ns.run("/tools/worm.js", 1, "avmnite-02h");
					while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
					pid = ns.run("singularity/join-faction.js", 1, "NiteSec");
					while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
					pid = ns.run("singularity/workForFaction.js", 1, "NiteSec", "Hacking Contracts", IDLE_FOCUS);
					while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
				}
			}
			else {
				//SING - STUDY IF LOW HACK LEVEL
				if (CURR_HACK_LEVEL < 50 || CURR_HACK_LEVEL < ns.getServerRequiredHackingLevel("CSEC")) {
					pid = ns.run("singularity/study.js", 1, "Rothman University", "Study Computer Science", IDLE_FOCUS);
					while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
				}
				// SING - GET FIRST PORT OPENER IF NOT BOUGHT ALREADY
				else if (!ns.fileExists("BruteSSH.exe")) {
					pid = ns.run("singularity/program.js", 1, "BruteSSH.exe", IDLE_FOCUS);
					while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
				}
				// SING - JOIN CSEC - WORK - PURCHASE AUGMENTS
				else {
					pid = ns.run("/tools/worm.js", 1, "CSEC");
					while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
					pid = ns.run("singularity/join-faction.js", 1, "CyberSec");
					while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
					pid = ns.run("singularity/workForFaction.js", 1, "CyberSec", "Hacking Contracts", IDLE_FOCUS);
					while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
				}
			}
			if (GLOBAL_VARS.FACTION_WORK) {
				pid = ns.run("singularity/workForFaction.js", 1, GLOBAL_VARS.FACTION_WORK, "Hacking Contracts", IDLE_FOCUS);
				while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
			}
		}

		// GANG
		if (!ns.isRunning("/v1/gang-combat.js", "home"))
			ns.run("/v1/gang-combat.js");
		await ns.sleep(1000);
		// BUY/UPGRADE PSERVERS
		if (!ns.isRunning("/v1/purchase-server.js", "home") && ns.getServerMoneyAvailable("home") > 11000000)
			ns.run("/v1/purchase-server.js");
		await ns.sleep(1000);
		// SING - GET STONKS RUNNING
		if (!ns.isRunning("/v1/stonks.js", "home"))
			ns.run("tools/start-stonks.js");
		await ns.sleep(1000);
		// DEPLOY FOR HOME AND PSERVERS
		if (!ns.isRunning("/v1/deploy.js", "home"))
			ns.run("/v1/deploy.js");
		await ns.sleep(1000);
		// START HACKNET MANAGER
		// if (!ns.isRunning("/v1/hacknetmanager.js", "home") && ns.getServerMoneyAvailable("home") > 25000000)
		// 	ns.run("/v1/hacknetmanager.js");

		// SING - WORM
		if (!ns.isRunning("/tools/worm.js", "home"))
			ns.run("/tools/worm.js");

		// DONE. WAIT AND DO IT AGAIN
		await ns.sleep(10000);
	}
}