/** @param {NS} ns */
import { FileHandler } from "/data/file-handler.js";
export async function main(ns) {
	const FILEHANDLER_PURCHASED_SERVERS = new FileHandler(ns, "/database/pservers.txt");	
	await FILEHANDLER_PURCHASED_SERVERS.write(ns.getPurchasedServers());
}