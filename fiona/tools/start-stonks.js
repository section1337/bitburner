/** @param {NS} ns */
export async function main(ns) {
	if (ns.getServerMoneyAvailable("home") > 10000000000 && ns.stock.purchaseWseAccount() && ns.stock.purchase4SMarketData() && ns.stock.purchaseTixApi() && ns.stock.purchase4SMarketDataTixApi())
		ns.run("/v1/stonks.js", 1);
}