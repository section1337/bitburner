/** @param {NS} ns **/
import { FileHandler } from "/data/file-handler.js";
export async function main(ns) {
	const FILEHANDLER_SERVERS = new FileHandler(ns, "/database/servers.txt");
	const FILEHANDLER_SERVERDATABASE = new FileHandler(ns, "/database/serverdatabase.txt");
	let servers = [];
	let serverdatabase = await FILEHANDLER_SERVERDATABASE.read();
	let serversToScan = ns.scan();
	const PSERVERS = ns.getPurchasedServers();
	while (serversToScan.length > 0) {
		await ns.sleep(10);
		const server = serversToScan.shift();
		if (!servers.includes(server)) {
			servers.push(server);
			serversToScan = serversToScan.concat(ns.scan(server));

			let serverstored = false;
			for (let s of serverdatabase) {
				if (s.name == server) {
					if (!s.root && ns.hasRootAccess(server))
						serverdatabase.shift(s);
					else {
						serverstored = true;
						break;
					}
				}
			}

			if (!serverstored) {
				let serverobject = {
					name: server,
					root: ns.hasRootAccess(server),
					minSec: ns.getServerMinSecurityLevel(server),
					maxMoney: ns.getServerMaxMoney(server),
					hackLev: ns.getServerRequiredHackingLevel(server),
					maxRam: ns.getServerMaxRam(server),
					type: "target"
				}
				if (server == "home")
					serverobject.type = "home";
				else if (PSERVERS.includes(server))
					serverobject.type = "pserver";

				serverdatabase.push(serverobject);
			}

			if (!ns.serverExists(server))
				continue;
			const S_CONTRACT_FILES = ns.ls(server, ".cct");
			if (S_CONTRACT_FILES.length > 0) {
				for (const S_CONTRACT of S_CONTRACT_FILES) {
					await ns.sleep(10);
					ns.exec("/v1/contractsolver.js", "home", 1, S_CONTRACT, server);
				}
			}
		}
	}
	await FILEHANDLER_SERVERS.write(servers);
	await FILEHANDLER_SERVERDATABASE.write(serverdatabase);
}