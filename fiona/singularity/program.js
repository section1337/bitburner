/** @param {NS} ns */
export async function main(ns) {
	ns.singularity.createProgram(ns.args[0],ns.args[1]);
}