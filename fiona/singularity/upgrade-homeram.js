/** @param {NS} ns */
export async function main(ns) {
	while(ns.singularity.getUpgradeHomeRamCost() < ns.getServerMoneyAvailable("home")) {
		if(ns.singularity.upgradeHomeRam())
			ns.tprint("INFO - Upgraded Home RAM!");		
		else break;
	}
	await ns.sleep(1);
}