/** @param {NS} ns */
export async function main(ns) {
	while (ns.singularity.getUpgradeHomeCoresCost() < ns.getServerMoneyAvailable("home")) {
		if (ns.singularity.upgradeHomeCores())
			ns.tprint("INFO - Upgraded Home Cores!");
		else break;
	}
	await ns.sleep(1);
}