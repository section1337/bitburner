/** @param {NS} ns */
import { FileHandler } from "/data/file-handler.js";

export async function main(ns) {
	const FILEHANDLER_SERVERS = new FileHandler(ns, "/database/serverdatabase.txt");
	const SERVERS = await FILEHANDLER_SERVERS.read();
	const SCRIPT_RAM = ns.getScriptRam("/shared/share.js");
	const HOME_SERV = SERVERS.find(serv => serv.name === "home");

	if (HOME_SERV == undefined)
		return;
	while (true) {
		const S_FREERAM = HOME_SERV.maxRam - ns.getServerUsedRam(HOME_SERV.name) - 50;
		if (S_FREERAM >= SCRIPT_RAM * 2) {
			const pid = ns.run("/shared/share.js", Math.floor(S_FREERAM / SCRIPT_RAM / 2));
			while (ns.getRunningScript(pid) != null) { await ns.sleep(100) }
		}
	}
}