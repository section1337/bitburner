/** @param {NS} ns */
import { FileHandler } from "/data/file-handler.js";

export async function main(ns) {
	const testi = {
		STORY_PROGRESS: true,
		FACTION_WORK: false,
		IDLE_FOCUS: false
	}
	const FILEHANDLER_GLOBAL_VARS = new FileHandler(ns, "/database/globalvars.txt");
	FILEHANDLER_GLOBAL_VARS.write(testi);
}