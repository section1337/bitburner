/** @param {NS} ns */
import { FileHandler } from "/data/file-handler.js";
export async function main(ns) {
	const fileHandler = new FileHandler(ns, "/database/serverdatabase.txt");
	const fileHandler2 = new FileHandler(ns, "/database/target.txt");
	const TARGETS = await fileHandler.read();
	const GREEDY_MODE = ns.getServerMoneyAvailable("home") < 1000000000;
	let primescore = 0;
	let primetarget = "foodnstuff";
	const OFFLINE_MODE = false;
	const MAX_MINUTES = 50;
	const HACK_LEV = ns.getHackingLevel();
	for (const target of TARGETS) {
		await ns.sleep(10);
		if (!ns.serverExists(target.name)
			|| target.maxMoney == 0
			|| target.hackLev > HACK_LEV
			|| !target.root
			|| ns.getWeakenTime(target.name) > 1000 * 60 * MAX_MINUTES)
			continue;
		let newscore;
		if(GREEDY_MODE)
			newscore = ns.getServerMoneyAvailable(target.name) / ns.getServerSecurityLevel(target.name);		
		else if (OFFLINE_MODE) {
			if(target.minSec * 1.2 > ns.getServerSecurityLevel(target.name))
				newscore = ns.getServerMoneyAvailable(target.name) / ns.getServerSecurityLevel(target.name);
		}
		else
			newscore = target.maxMoney / target.minSec;
		if (newscore > primescore) {
			primescore = newscore;
			primetarget = target;
		}		
	}
	await fileHandler2.write(primetarget);
}