/** @param {NS} ns */
export async function main(ns) {
	const FACTION_AUGMENTS = ns.singularity.getAugmentationsFromFaction(ns.args[0]);
	const MY_AUGMENTS = ns.singularity.getOwnedAugmentations(true);
	let augmentToBuy;
	let highestPrice = 0;
	for (let augment of FACTION_AUGMENTS) {
		if (!MY_AUGMENTS.includes(augment)) {
			const AUG_COST = ns.singularity.getAugmentationPrice(augment);
			const PREREQ = ns.singularity.getAugmentationPrereq(augment);
			if(AUG_COST > highestPrice && (PREREQ != [] || MY_AUGMENTS.includes(PREREQ))) {
				highestPrice = AUG_COST;
				augmentToBuy = augment;
			}
		}
	}
	if(highestPrice > 0) {
		ns.singularity.purchaseAugmentation(ns.args[0],augmentToBuy);
	}
}