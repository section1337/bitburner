/** @param {NS} ns */
export async function main(ns) {
	ns.singularity.workForFaction(ns.args[0],ns.args[1],ns.args[2]);
	await ns.sleep(1);
}